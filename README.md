### Instructions:
#### Assessment Sample 1:
* [ ] Open up IDE at location of Tozny-Assessment-S1
* [ ] Type into CLI the following command
  * [ ] To reveal answer to problem 1:
    * [ ]  python inventory-manager.py problem1
  * [ ] To reveal answer to problem 2:
    * [ ]  python inventory-manager.py problem2
  * [ ] To reveal answer to problem 3:
    * [ ]  python inventory-manager.py problem3
  * [ ] To reveal answer to problem 4:
    * [ ]  python inventory-manager.py problem4

#### Assessment Sample 2:
* [ ] Open up IDE at location of Tozny-Assessment-S2
* [ ] Register clients using crypto-registration.py
  * [ ] Make sure to change name variable for each user
  * [ ] Create username_credentials.json per user by running file
    * [ ] Example: alicia_credentials.json
* [ ] Type into CLI the following commands and then follow each prompt
  * [ ]  python .\crypto-game.py move
  * [ ]  python .\crypto-game.py move
  * [ ]  python .\crypto-game.py judge
  * [ ] python .\crypto-game.py result