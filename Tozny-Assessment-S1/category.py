import json

with open("input.json", "r") as f:
    data = json.load(f)
# print(data)
# print(data.items())
# print(data['media'][0]['price'])
# This would probably be easier if I set up my models using django react or something later on if I wanted to make a page

def order_by_categories(json_file):
    json_copy1 = json_file.copy()
    dict1 = {}
    inventory_items = json_copy1['inventory']
    # print(inventory_items)
    for item in inventory_items:
        if item['type'] not in dict1.keys():
            dict1[item['type']] = [item]
        else:
            dict1[item['type']] += [item]
    return dict1
categories = order_by_categories(data)

# Next time just import copy and use .deepcopy()
json1 = categories.copy()
json2 = categories.copy()
json3 = categories.copy()
json4 = categories.copy()
