from category import json1, json2, json3, json4
import typer

#To implement typer Python CLI app, uncomment @app.command() lines and line below
app = typer.Typer()


# 1. What are the 5 most expensive items from each category?
    # If we were to manipulate our most recent dictionary, we could create a list of only prices, sort it, and slice for the last 5 for us to search for in the type dict and return those objects in our newest type dict with 5 most expensive objects.
    # Return a dictionary of categories with keys as a list of objects
@app.command()
def problem1():
    results = {}
    types = []
    #Consider using the map method instead to create a collection? Maybe apply it to my original categories dict?
    for typeof in json1:
        types.append(typeof)
        results[typeof] = json1[typeof].copy()

    for type in types:
        prices = []
        for cat in results[(type)]:
            prices.append(cat['price'])
        try:
            for i in range(len(results[type])):
                if results[type][i]['price'] not in (sorted(prices)[-1:-6:-1]):
                    del results[type][i]
        except IndexError:
            pass
    print(results)
        # return results
# five_most = problem1()
# print('#1: ', five_most)


# 2. Which cds have a total running time longer than 60 minutes?
# So cd time goes by second, so per CD, we could create a set where we goto 'tracks' then added up each tracks "seconds" to get the total time in minutes.
# We don't technically need minutes if we just filter by the amount of seconds equal to an hour which is 3600 seconds (pro: no need to convert to minutes, con: harder to potentially read)
# We can create a list of objects with with adequate runtime
@app.command()
def problem2():
    results = []
    # print(json_file['cd'])
    for album in json2['cd']:
        sum = 0
        for track in album['tracks']:
            sum += track['seconds']
            # print(sum)
        if sum > 3600:
            results.append(album)
    print(results)
    # return results
# long_cds = problem2()
# print('#2: ', long_cds)


# 3. Which authors have also released cds?
# for some reason, I can't do this function without having it come after the first question? I tried to make a copy of a dict using the copy method but haven't had any luck
# print(json_file3)
@app.command()
def problem3():
    mutual_authors_var = []
    mutual_authors_objects_list = []
    cd_authors = []
    for cds in json3['cd']:
        cd_authors.append(cds['author'])

    book_authors = []
    for books in json3['book']:
        book_authors.append(books['author'])

    for author in book_authors:
        if author in cd_authors:
            mutual_authors_var.append(author)

    # consider using filter method?
    for author1 in mutual_authors_var:
        # print(author1)
        for author_var in json3['cd']:
            if author1 == author_var['author']:
                mutual_authors_objects_list.append(author_var)

    print(mutual_authors_objects_list)
    # return mutual_authors_objects_list
# mutual_authors = problem3()
# print('#3: ', mutual_authors)


# 4. Which items have a title, track, or chapter that contains a year.
# Do I want to return a list of objects in Python or as a new object of objects per year?
# I should double check to see if track, and chapter to see if they're mutually exclusive
# Ask Katie- There appears to be title's for everything, but DVDs don't have tracks or chapters, so then do I just want a list of books and cds?
# Ask Katie- Confirmation question, should I do something that includes a rickroll in the json file or would that be too unprofessional to the engineers I would interview with?
# validate year

# How far ahead am I going to have to deal with this in regards to outliers such as year 100000 AD?
def valid_year(year):
    if year and year.isdigit():
        if int(year) >= 1000 and int(year) <= 9999:
            return True

@app.command()
def problem4():
    with_year_list = []
    items = json4
    # Go through categories
    for item in items:
        # going through the titles
        # print(item)
        # print(items[item])
        # Go through items in categories
        for i in range(len(items[item])):
            # come up with a way to make sure we have "2001: an" accepted into the list
            if "title" in items[item][i]:
                # print(items[item][i]['title'])
                for n in range(len(items[item][i]['title'])-3):
                    if (items[item][i]['title'][n]).isnumeric() == True and (items[item][i]['title'][n+1]).isnumeric() == True and (items[item][i]['title'][n+2]).isnumeric() == True and (items[item][i]['title'][n+3]).isnumeric() == True:
                        possible_year = items[item][i]['title'][n:n+4:1]
                        # used this to double check if it's a year thingy.
                        if valid_year(possible_year) == True:
                            with_year_list.append(items[item][i])

                if valid_year(items[item][i]['title']) == True and items[item][i] not in with_year_list:
                    with_year_list.append(items[item][i])

            if "tracks" in items[item][i]:
                for j in range(len(items[item][i]['tracks'])):
                    if valid_year(items[item][i]['tracks'][j]['name']) == True and items[item][i] not in with_year_list:
                        with_year_list.append(items[item][i])

            if "chapters" in items[item][i]:
                for k in range(len(items[item][i]['chapters'])):
                    if valid_year(items[item][i]['chapters'][k]) == True and items[item][i] not in with_year_list:
                        with_year_list.append(items[item][i])
    print(with_year_list)                  
    # return with_year_list
# as_years = problem4()
# print('#4: ', as_years)

# input("Which problem would you like to answer? (problem1, problem2, problem3, problem4")

# This dunder method is if we use typer for making our python cli app
if __name__ == "__main__":
    app()





