import e3db
# So far from what I can understand, this is just used to register clients under a specific token
# So maybe then Tokens are classes of users, and clients are people under each class?

token = '55633dcbf73ab5da5965fe667c3b6aac806def4817b5a9aec9387be9a6841742'
print("Using Registration Token: {0}".format(token))

client_name = 'Alicia'
print("Client Name: {0}".format(client_name))

public_key, private_key = e3db.Client.generate_keypair()
print("Public Key: {0}".format(public_key))
print("Private Key: {0}".format(private_key))

# client_info = e3db.Client.register(token, client_name, public_key)
# Register your client
client_info = e3db.Client.register(token, client_name, public_key, private_key=private_key, backup=True)
print("Client Information: ", client_info)

api_key_id = client_info.api_key_id
api_secret = client_info.api_secret
client_id = client_info.client_id

print("Client ID: {0}".format(client_id))
print("API Key ID: {0}".format(api_key_id))
print("API Secret: {0}".format(api_secret))

# ---------------------------------------------------------
# Usage
# ---------------------------------------------------------

# Once the client is registered, you can use it immediately to create the
# configuration used to instantiate a Client that can communicate with
# e3db directly.

# Assuming your credentials are stored as defined constants in the
# application, pass them each into the configuration constructor as
# follows:

config = e3db.Config(
    client_info.client_id,
    client_info.api_key_id,
    client_info.api_secret,
    public_key,
    private_key
)

# Optionally, if you want to save this Configuration to disk, do the following:
# config.write()

# Instantiate your client to communicate with TozStore
# Now create a client using that configuration.
client = e3db.Client(config())

# Configuration files live in ~/.tozny and you can have several
# different "profiles" like *dev* and *production*.

# Write ~/.tozny/dev/e3db.json profile
# config.write('dev')

# From this point on, the new client can be used as any other client to read
# write, delete, and query for records. See the `simple.py` documentation
# for more complete examples ...