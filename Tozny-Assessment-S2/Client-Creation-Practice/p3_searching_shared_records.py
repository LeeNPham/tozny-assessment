import e3db
from e3db.types import Search

# Clarence = {
#     "version": "1",
#     "client_id": "abe33d2b-b1fc-4b35-9719-2a4b0e060492",
#     "api_key_id": "145b1def600b41afeff85e410d224f1010ce250b05ce29c455e64ca21558e3eb",
#     "api_secret": "eee76f86b198f4286819182606b1683380005365d0854b1a21c0357a593ace32",
#     "public_key": "coEiNQEB4aXZJDvZROlXkCV84LFbmFLaTW2l8v1-F2I",
#     "private_key": "ADyYvmmcYXjlo-ycklmXkUby3kIahzbkuR07uQWdU1A",
#     "api_url": "https://api.e3db.com",
#     "client_email": ""
# }

third_party_config = e3db.Config(
    "abe33d2b-b1fc-4b35-9719-2a4b0e060492",
    "145b1def600b41afeff85e410d224f1010ce250b05ce29c455e64ca21558e3eb",
    "eee76f86b198f4286819182606b1683380005365d0854b1a21c0357a593ace32",
    "coEiNQEB4aXZJDvZROlXkCV84LFbmFLaTW2l8v1-F2I",
    "ADyYvmmcYXjlo-ycklmXkUby3kIahzbkuR07uQWdU1A",
    api_url = "https://api.e3db.com",
)

player_1 = '473969b4-ba9b-4956-9de3-5d662f808def'
player_2 = '0417d637-1e40-4c84-9010-6ac89ac3ae93'

print(third_party_config)
third_party_client = e3db.Client(third_party_config())

record_type = "move"
search_filter = Search(include_all_writers=True, include_data=True) \
    .match(condition="AND", record_types=['move'], writers=[player_2])

results = third_party_client.search(search_filter)
for record in results:
    print(record.to_json())