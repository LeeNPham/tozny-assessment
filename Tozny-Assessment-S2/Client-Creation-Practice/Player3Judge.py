import e3db
from e3db.types import Search
from p2_send_data import Clarence, Alicia, Bruce, record_type


client = e3db.Client(Clarence)
# You can also find the record id inside of clients, per record, under the record type


judge_config = e3db.Config(
    "e92b6623-b7ac-4e05-bb53-436586461c34",
    "ecf6c923fc2b2f13bd85cf8aac6b35c8be19288009ef20f720bc14bfe00510f1",
    "2089f3980f31ed3f7d27ee46d4f06bb0ef7103c224a3860fde760d1e4433ca19",
    "TAQCCeKvEVsRsaWJOQNRp946mKWr3tbk0uzTYmjEUhc",
    "27EbQS8wbvyWXvxq9rog71uiCm7QReVx7WkkH0Tp2nw",
    api_url="https://api.e3db.com",
)

# player_1 = "9adaa602-6b5d-4008-a9e5-4e6f7f73da3e"
player_2 = "f503ba45-8b79-4f7a-891d-1585c9951d6d"

print(judge_config)
third_party_client = e3db.Client(judge_config())


# search_filter1 = Search(include_all_writers=True, include_data=True) \
#     .match(condition="AND", record_types=['move'], writers=[player_1])

search_filter2 = Search(include_all_writers=True, include_data=True) \
    .match(condition="AND", record_types=['move'], writers=[player_2])

results = third_party_client.search(search_filter2) # Argument will be either search_filter1 or search_filter2
for record in results:
    # print(record.to_json())

    record_id = (record.to_json())['meta']['record_id']
    # print ("Record ID: ",record_id)

    # round_writer = (record.to_json())['meta']['plain']['Name']
    # print ("User Name: ",round_writer)

    # round_number = (record.to_json())['meta']['plain']['Round']
    # print ("Round Number: ",round_number)

    # user_id = (record.to_json())['meta']['user_id']
    # print ("User ID: ", user_id)

    # round_move = (record.to_json())['data']['Move']
    # print ("Round Move: ", round_move)

    temp_record = client.read(record_id)
    # print (temp_record.to_json())
    print ("Record of Round ({1}) move by ({2}): {0}".format(temp_record.data['Move'], temp_record.meta.plain['Round'], temp_record.meta.plain['Name']))

# Share winner back when running judge function
# create function to determine winner based on record_id (to rea), round_writer, round_number, user_id, round_move

# Reference to get data points

# new_record = client.read(record_id)
# print "Record: {0} {1}".format(new_record.data['Storage'], new_record.data['Unlock Code'])


#   original_record = client.read(record_id)
#   original_record.data['name'] = 'Mr. Jon Snow'
#   updated_record = client.update(original_record)
#   print "Record: {0} {1}".format(updated_record.data['name'])