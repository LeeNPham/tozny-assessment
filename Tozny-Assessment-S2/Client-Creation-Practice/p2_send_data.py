import e3db
# Reference for how to send out data
Alicia = {
    "version": "1",
    "client_id": "9adaa602-6b5d-4008-a9e5-4e6f7f73da3e",
    "api_key_id": "6b71784fc754f8114ca23a25a24533053e70cd33b38babc620b43251e445b1a6",
    "api_secret": "9a2d6668b0595743e17c2aa6af06e2b939f3b06422113042c8530de14d314bd7",
    "public_key": "Y1SBRt7p2fNQLl6wPCSOsdcTuQRy1lH_gjw-ApS7yGM",
    "private_key": "01iZj0dIL20ivCbS6StkgisATStVXJbbykvrdxLLrJY",
    "api_url": "https://api.e3db.com",
    "client_email": ""
}

Bruce = {
    "version": "1",
    "client_id": "f503ba45-8b79-4f7a-891d-1585c9951d6d",
    "api_key_id": "1d35d69e7e3f3ffc6d5ea65d045de1105091227c7c97da486f3af0f8844ff956",
    "api_secret": "c8af334f2a9b2069acced821f1e61aa3485281583b1341a285999b89e1ef8df1",
    "public_key": "VYBzq7yDTaiV5cjdzFE8Hsq7blqYYtTqj5DC88zsUxk",
    "private_key": "1DgAHyJGzAxB6PmwcENnnT7ZHH2zk4oaJtmABSR5mEs",
    "api_url": "https://api.e3db.com",
    "client_email": ""
}

Clarence = {
    "version": "1",
    "client_id": "e92b6623-b7ac-4e05-bb53-436586461c34",
    "api_key_id": "ecf6c923fc2b2f13bd85cf8aac6b35c8be19288009ef20f720bc14bfe00510f1",
    "api_secret": "2089f3980f31ed3f7d27ee46d4f06bb0ef7103c224a3860fde760d1e4433ca19",
    "public_key": "TAQCCeKvEVsRsaWJOQNRp946mKWr3tbk0uzTYmjEUhc",
    "private_key": "27EbQS8wbvyWXvxq9rog71uiCm7QReVx7WkkH0Tp2nw",
    "api_url": "https://api.e3db.com",
    "client_email": ""
}

# client = e3db.Client(Bruce)
# # uncomment line above to encrypt and send information to tozny server

record_type = 'move'
# # This is the encrypted Data~
# # I need to see how I can access the encrypted data,
# # maybe see if there's profile types that allow for me to see certain user type accounts?

# data = {
#     'Move': 'Scissors',
# }

# metadata = {
#     'Round': '1',
#     'Name': "Bruce"
# }

# record = client.write(record_type, data, metadata)

# print(record)
# print('Wrote record {0}'.format(record.meta.record_id))

# # Sharing Records
# judge_client_id = Clarence['client_id']
# client.share(record_type, judge_client_id)
