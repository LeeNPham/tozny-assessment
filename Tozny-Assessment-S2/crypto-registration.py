import e3db
import json

# create token and corresponding clients via the TozStore dashboard or through here
token = '55633dcbf73ab5da5965fe667c3b6aac806def4817b5a9aec9387be9a6841742'
print("Using Registration Token: {0}".format(token))

client_name = input("What is the name of this player or judge? ").capitalize()
print("Client Name: {0}".format(client_name))

# client_email = input("What is the email for this person? ")

public_key, private_key = e3db.Client.generate_keypair()

# client_info = e3db.Client.register(token, client_name, public_key)
# Figure out how we can manage the email for a client? Maybe look into overwriting credentials to update email?
# Register your client
client_info = e3db.Client.register(
    token, client_name, public_key, private_key=private_key, backup=True)
print("Client Information: ", client_info)

api_key_id = client_info.api_key_id
api_secret = client_info.api_secret
client_id = client_info.client_id
api_url = "https://api.e3db.com"

# /credentials
#   /players
#   /judges
# credentials.json

# Write a json file containing the credentials 
# (typically we want to use config.write() though but the file path was wonky when I first tried it out)
# change to version 2 if there's a public_signing_key and private_signing_key
data = {
    "version": "1",
    "client_id": f"{client_id}",
    "api_key_id": api_key_id,
    "api_secret": api_secret,
    "public_key": public_key,
    "private_key": private_key,
    "api_url": api_url,
    "client_email": ""
}

with open(f"{client_name.lower()}_credentials.json", "w") as outfile:
    json.dump(data, outfile)

# print(data)
print("Copy these credentials to make a json file")
print("{")
print("'version': '1'")
print("'client_id': '{0}'".format(client_id))
print("'api_key_id': '{0}'".format(api_key_id))
print("'api_secret': '{0}'".format(api_secret))
print("'public_key': '{0}'".format(public_key))
print("'private_key': '{0}'".format(private_key))
print("'api_url': '{0}'".format(api_url))
print("'client_email': ''")
print("}")

# ---------------------------------------------------------
# Usage
# ---------------------------------------------------------

# Once the client is registered, you can use it immediately to create the
# configuration used to instantiate a Client that can communicate with
# e3db directly.

# Assuming your credentials are stored as defined constants in the
# application, pass them each into the configuration constructor as
# follows:

config = e3db.Config(
    client_info.client_id,
    client_info.api_key_id,
    client_info.api_secret,
    public_key,
    private_key
)


# Trying to add email to the configuration model and back it up in the credentials
# Looked at register() and couldn't find references to an email
# Maybe try and use client_info.client_email for the configuration????
# config = e3db.Config(
#     client_id,
#     api_key_id,
#     api_secret,
#     public_key,
#     private_key, 
#     client_email
# )


# Optionally, if you want to save this Configuration to disk, do the following:
# config.write()

# Instantiate your client to communicate with TozStore
# Now create a client using that configuration.
client = e3db.Client(config())

# Configuration files live in ~/.tozny and you can have several
# different "profiles" like *dev* and *production*.

# Write ~/.tozny/dev/e3db.json profile
# config.write('dev')

# From this point on, the new client can be used as any other client to read
# write, delete, and query for records. See the `simple.py` documentation
# for more complete examples ...
