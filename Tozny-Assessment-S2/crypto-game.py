import e3db
from e3db.types import Search
import sys
import os
import json

# started down a rabbit hole to make life easier for me but because the keys should be private 
# and only the user specific to the client id should be using it, I'm going to not do what I had explored below
    # maybe generalize each of the players and then when registering players, have their names = the key to the json
    # reference the key/name thing in a for loop for the number of players and judges stored. 
    # so maybe do a length to be looped over a players and judges list, and then
    # see if a players name entry = the name of a specific file (so in this case, maybe we should consider a username?)
    # if so, how can we make sure that the username values are going to be unique and immutable when registering new clients
    # pseudo for fun :D
    # with open("player_credentials.json", r) as f:
    #   player_credentials = json.load(f)
    #   for name in player in 
    #   client(1) = player_credentials[name]
# - rename flag to something meaningful; maybe even invert the logic if it reads nicer (break case?)
# - If I had more time for file separation from the main game file, there seems to be the following patterns:
#   - Reading & Writing (they could be separate or grouped together)
#   - Judging? Maybe?

# If I had more time, I would wanna try and create a realm, then create different departments, and roles, 
# It would be cool to use this one python gaming module that adds little sprites and animations to what i input as well

with open("alicia_credentials.json", "r") as f:
    alicia_credentials = json.load(f)
    client_1 = alicia_credentials['client_id']


with open("bruce_credentials.json", "r") as f:
    bruce_credentials = json.load(f)
    client_2 = bruce_credentials['client_id']


with open("clarence_credentials.json", "r") as f:
    clarence_credentials = json.load(f)
    client_3 = clarence_credentials['client_id']

# Generate client_ids first and save them to the files named above using crypto-registration.py

# check number of records for a given search
def round_check(res):
    i = 0
    for rec in res:
        i += 1
    return i

# function assumes there will only be one record for any given round
def results(res, key):
    for rec in res:
        value = rec.data[key]
    return value

# function checks if a round exists when a player is choosing a round to make a move for
def check_for_round(client, player, roundnum):
    break_case = True

    while break_case == True:
        round_query = Search(include_data=True, include_all_writers=True). \
            match(condition="AND", strategy="EXACT", values=[roundnum, player])

        search = client.search(round_query)
        num = round_check(search)
        if num == 0:
            break_case = False
        else:
            roundnum = input(
                "This round already exists, please enter new round number: ")
    return roundnum

#  function makes sure player move input is correct
def check_move(move2chk):
    flag = True

    while flag == True:
        if move2chk == 'rock' or move2chk == 'scissors' or move2chk == 'paper':
            flag = False
        else:
            print("incorrect input(note: lowercase only)")
            move2chk = input("Whats your move, rock, paper, or scissors?: ")
    return move2chk

# function initializes client credentials and makes sure path exists
def check_credentials(file_path):
    credentials_path = file_path  # your e3db credentialss
    if os.path.exists(credentials_path):
        client = e3db.Client(json.load(open(credentials_path)))
        return client
    else:
        print("Filepath doesn't exist")
        quit()

# function submits a move for the player for a given round
def submit_move(client, client_name, move, roundnum):
    record_type = "moves"

    data = {'Move': move}
    meta_data = {
        'Round': roundnum,
        'Name': client_name
    }

    client.write(record_type, data, meta_data)
    if roundnum == "1" or roundnum == 1:
        # only has to be ran during first round
        client.share(record_type, client_3)
    print("Round {0} Move {1} submitted for {2} ".format(
        roundnum, move, client_name))

# function checks if a round has been judged, used when judge inputs which round he/she wants to judge
def is_round_judged(client, roundnum):
    break_case = True
    while (break_case == True):
        rnd_query = Search(include_data=True, include_all_writers=True). \
            match(condition="AND", strategy="EXACT",
                  record_types=["Result"], values=[roundnum])

        search = client.search(rnd_query)
        num = round_check(search)

        if num == 0:
            break_case = False
        else:
            roundnum = input(
                "This round has been judged, please enter new round number: ")
    return roundnum

# used by player to see if a round has been judged, when choosing which round to get a result from
def player_round_check(client, resultRound):
    query = Search(include_data=True, include_all_writers=True). \
        match(condition="AND", strategy="EXACT", record_types=["Result"], values=[resultRound])

    rnd_result = client.search(query)
    num1 = round_check(rnd_result)

    if num1 == 0:
        print("This round has yet to be judged.")
        quit()
    return rnd_result

# this function searches for moves based on player name and round number meta-data, used by judge
def search_for_moves(client, player_name, roundnum):
    query = Search(include_data=True, include_all_writers=True). \
        match(condition="AND", strategy="EXACT", values=[roundnum, player_name])
    # judge searches for records from Alicia and Bruce for the round specified
    result = client.search(query)
    return result

# checks if a move had been made by both players for a given round, used by judge before judging any round
def check_for_moves_made(player1_results, player2_results):
    num1 = round_check(player1_results)
    num2 = round_check(player2_results)
    if num1 == 0 or num2 == 0:
        print("One or two players have yet to make a move for this round")
        quit()

# create hashmap and iterate over the moves to return the winner per player1s move (tip from Katie)
# this function judges the round
def judge_round(player1, player2, move1, move2):
    # player1 move key, player2 move secondary key, return value
    player1_move = {
    "rock": {"rock": "Draw", "paper": player2, "scissors": player1},
    "paper": {"rock": player1, "paper": "Draw", "scissors": player2},
    "scissors": {"rock": player2, "paper": player1, "scissors": "Draw"}
    }
    
    return player1_move[move1][move2]

    # if move1 == move2:
    #     return "Draw"

    # elif (move1 == "rock") and (move2 == "paper"):
    #     return player2

    # elif (move1 == "rock") and (move2 == "scissors"):
    #     return player1

    # elif (move1 == "paper") and (move2 == "rock"):
    #     return player1

    # elif (move1 == "paper") and (move2 == "scissors"):
    #     return player2

    # elif (move1 == "scissors") and (move2 == "rock"):
    #     return player2

    # elif (move1 == "scissors") and (move2 == "paper"):
    #     return player1

# this function submits the round from the judge
def submit_winner(client, player, roundnum):
    record_type = "Result"

    judge_data = {
        "Winner": player
    }
    meta = {
        "Round": roundnum
    }

    client.write(record_type, judge_data, meta)
    if roundnum == "1":
        client.share(record_type, client_1)
        client.share(record_type, client_2)

    print("Round Judged: ", roundnum)

#------------------------------------------------
# Start of actual program
#------------------------------------------------

# commonly called arguments from sys
action = sys.argv[1].capitalize()
# credential_file = sys.argv[2] trying to not have to pass in secondary arg (delete later if successful in creating credential file in action conditions)
client_name1 = "Alicia"
client_name2 = "Bruce"
client_name3 = "Clarence"
players = [client_name1, client_name2]
judges = [client_name3]


# Do later
# I should either have the entered name load the associated credentials for the name
# or maybe check not only the name but the credentials to make sure that they're the properly correlating profiles
# Something to improve on, extract various name strings and turn them into variables based on their credentials to reference throughout the following code

# creating moves record per player
if action == 'Move':
    name = input('Enter the name of the player: ').capitalize()
    # currently just double checks that the name is the same as the one in the credentials
    if (name in players):
        # get credentials
        credential_file = f"./{name.lower()}_credentials.json"
        client = check_credentials(credential_file)
        # ask for round to submit move for
        round_number_input = input("Enter the round number: ")
        # get round number to create move for
        roundNum = check_for_round(client, name, round_number_input)
        # ask for move
        move_input = input("Whats your move, rock, paper, or scissors?: ").lower()
        # get move
        move = check_move(move_input)
        # submit move
        submit_move(client, name, move, roundNum)
    else:
        print("Player does not exist")

# judging the results
elif action == "Judge":
    credential_file = f"./{client_name3.lower()}_credentials.json"
    client3 = check_credentials(credential_file)

    verification = input("Please verify your name, Judge: ").capitalize()

    if verification == client_name3:

        temp = input("Enter number of round you want to judge: ")
        # get round to judge
        round2judge = is_round_judged(client3, temp)

        # search for move made by both players
        results1 = search_for_moves(client3, client_name1, round2judge)
        results2 = search_for_moves(client3, client_name2, round2judge)

        # check if moves have been made by both players
        check_for_moves_made(results1, results2)

        # set moves from results
        a_move = results(results1, 'Move')
        b_move = results(results2, 'Move')

        print(f"{client_name1}'s move: ", a_move)
        print(f"{client_name2}'s move: ", b_move)

        # submit judged round
        winner = judge_round(client_name1, client_name2, a_move, b_move)
        submit_winner(client3, winner, round2judge)

# reading the results
elif action == "Result":

    # get round number and player name
    resultRound = input('Enter round number to get result of: ')
    name = input("Enter your name: ").capitalize()

    credential_file = f"./{name.lower()}_credentials.json"
    client = check_credentials(credential_file)

    if name in players or name in judges:
        # check if round has been judged
        result = player_round_check(client, resultRound)

        # get and print winner
        winner = results(result, 'Winner')
        print("{0} read result for round {1}, Winner: {2}".format(name, resultRound, winner))

    else:
        print("Player does not exist")

else:
    print('Only parameters "move" , "result" , or "judge" are available')
