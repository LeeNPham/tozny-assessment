1. - register players and judge using input and maybe python cli app? (register_player.py, register_judge.py)
2. - write program to submit move to be judged (submit_move.py)
3. - write program to judge moves from player1 and 2 for a given round and submit a record of the moves and who won (judge.py)
4. - write program to read what the judge wrote for a specific round (winner.py)


<h2><u>Take Home & Peer Review Coding Assessment</u></h2>
<h4>Process</h4>

As part of the Tozny Software Intern interview process we need to assess both your ability to
use code to solve problems, and to communicate and collaborate with other engineers in
implementing and explaining a solution involving code. 

<h5><u>In order to do that we ask each candidate to;</u></h5>

● Review the below sample problems to implement

● Implement a working solution to the problem, reaching out over email as needed to
clarify requirements or ask for advice

● Within two weeks of receiving this email, share via email a copy of the source code to
your solution, or a link to a Github repository containing the source code of your solution

● Schedule a 1hr meeting with the Tozny engineering team to do a live code review of your
solution, and collaborate on how you would modify the program if given more time or
different requirements

○ Use this link to schedule the above meeting after you have submitted the source
code to your solution 
https://calendly.com/toznykatie/intern-interviews?month=2022-12

==<h5>Please don’t hesitate to reach out over email if you have any questions!</h5>==

---

<h2><u>Sample Problems</u></h2>

### Inventory Manager
Your task is to write functions in the programming language of your choice which can operate on
the example JSON value below to answer the following questions:

- [x] 1. What are the 5 most expensive items from each category?
For example a javascript function that receives json input as a parameter and returns an
object consisting of 5 list of items, where each list contains the 5 most expensive items for a
given category (e.g. cd, book, or dvd) based on the inventory list input passed to the function

- [x] 2. Which cds have a total running time longer than 60 minutes?
- [x] 3. Which authors have also released cds?
- [x] 4. Which items have a title, track, or chapter that contains a year.

####==Use this Json File==

[{
        "price": 15.99,
        "chapters": [
            "one",
            "two",
            "three"
        ],
        "year": 1999,
        "title": "foo",
        "author": "mary",
        "type": "book"
    },
    {
        "price": 14.99,
        "chapters": [
            "one",
            "two",
            "three"
        ],
        "year": 1999,
        "title": "foo",
        "author": "had",
        "type": "book"
    },
    {
        "price": 13.99,
        "chapters": [
            "one",
            "two",
            "three"
        ],
        "year": 1999,
        "title": "1984",
        "author": "a",
        "type": "book"
    },
    {
        "price": 12.99,
            "chapters": [
            "one",
            "two",
            "three"
            ],
        "year": 1999,
        "title": "foo",
        "author": "little",
        "type": "book"
    },
    {
        "price": 11.99,
            "chapters": [
            "one",
            "two",
            "three"
            ],
        "year": 1999,
        "title": "foo",
        "author": "lamb",
        "type": "book"
    },
    {
        "price": 10.99,
            "chapters": [
            "one",
            "two",
            "three"
            ],
        "year": 1999,
        "title": "foo",
        "author": "joseph",
        "type": "book"
    },
    {
        "price": 11.99,
        "minutes": 90,
        "year": 2004,
        "title": "2001: An",
        "director": "alan",
        "type": "dvd"
    },
    {
        "price": 11.99,
        "minutes": 90,
        "year": 2004,
        "title": "Affair",
        "director": "alan",
        "type": "dvd"
    },
    {
        "price": 11.99,
        "minutes": 90,
        "year": 2004,
        "title": "To",
        "director": "alan",
        "type": "dvd"
    },
    {
        "price": 11.99,
        "minutes": 90,
        "year": 2004,
        "title": "Remember",
        "director": "alan",
        "type": "dvd"
    },
    {
        "price": 15.99,
        "tracks": [
            {
                "seconds": 180,
                "name": "one"
            },
            {
                "seconds": 200,
                "name": "two"
        }
        ],
        "year": 2000,
        "title": "baz",
        "author": "joan",
        "type": "cd"
    },
    {
        "price": 15.99,
        "tracks": [
            {
                "seconds": 180,
                "name": "one"
            },
            {
                "seconds": 200,
                "name": "two"
            }
        ],
        "year": 2000,
        "title": "baz",
        "author": "joseph",
        "type": "cd"
    },
    {
        "price": 16.99,
        "tracks": [
            {
                "seconds": 1900,
                "name": "one"
            },
            {
                "seconds": 1800,
                "name": "two"
            }
        ],
        "year": 2000,
        "title": "baz",
        "author": "juan",
        "type": "cd"
    },
    {
        "price": 17.99,
        "tracks": [
            {
                "seconds": 1800,
                "name": "2015"
            },
            {
                "seconds": 2800,
                "name": "two"
            }
        ],
        "year": 2000,
        "title": "1989",
        "author": "john",
        "type": "cd"
    },
    {
        "price": 18.99,
        "tracks": [
            {
                "seconds": 1800,
                "name": "one"
            },
            {
                "seconds": 2800,
                "name": "two"
            }
        ],
        "year": 2000,
        "title": "If you're reading this",
        "author": "john",
        "type": "cd"
    },
    {
        "price": 19.99,
        "tracks": [
            {
                "seconds": 1800,
                "name": "one"
            },
            {
                "seconds": 2800,
                "name": "two"
            }
        ],
        "year": 2000,
        "title": "It's too late",
        "author": "john",
        "type": "cd"
    },
    {
        "price": 20.99,
        "tracks": [
            {
                "seconds": 1800,
                "name": "one"
            },
            {
                "seconds": 2800,
                "name": "two"
            }
        ],
        "year": 2000,
        "title": "Nothing was the same",
        "author": "drake",
        "type": "cd"
    }]


---

### Rock, Paper, Scissors
Write a program or collection of scripts using a Tozny SDK to play a game of Rock Paper Scissors involving three parties with the following constraints

##### <u>1.) Player Alicia</u>
- [x] Submits a move for a round of Rock Paper Scissors by using a Tozny SDK to write an encrypted version of their move that is able to be decrypted ONLY by Judge Clarence
- [x] Uses a Tozny SDK to read the winner of the round as declared by Judge Clarence

##### <u>2.) Player Bruce</u>
- [x] Submits a move for a round of Rock Paper Scissors by using a Tozny SDK to write an encrypted version of their move that is able to be decrypted ONLY by Judge Clarence
- [x] Uses a Tozny SDK to read the winner of the round as declared by Judge Clarence

##### <u>3.) Judge Clarence</u>
- [x] Uses a Tozny SDK to decrypt moves from Alicia and Bruce for a round
- [x] Uses a Tozny SDK to write an encrypted version of the winner of a round
- [x] Can be read by BOTH Alicia and Bruce, and Judge Clarence

> The final output for the solution should take the form of one or more executable programs 

(e.g. a script if using the Python SDK, or a binary if using the Java SDK) 
that could be used by the three parties to play a game of Rock Paper Scissors.

---

##### For example you might
- [ ] write one program that can be used by the players to submit a move for a round,
```$ python3 play.py round=1 name=alicia move=rock```

```tozny-client-credentials-filepath=./alicia_creds.json```

> Round “1” Move “Rock” for “Alicia” submitted!

- [ ] another program for the Players to read and print out the winner for a round,
```$ python3 winner.py round=1 tozny-client-credentials-filepath=./bruce_creds.json```

> Round “1” Winner “Alicia”

- [ ] and another program for the Judge to use to read the moves for a round and write an encrypted version of the winner for a given round.

```$ python3 judge.py round=1```

```tozny-client-credentials-filepath=./clarence_creds.json```

> Round “1” Judged!

- [x] Or you could write one program that based on the parameters passed to it can be used
for any of the above.

```$./crypto-game.py mode=move round=1 move=rock```

```tozny-client-credentials-filepath=./alicia_creds.json```

> Round “1” Move “Rock” for “Alicia” submitted!

Or any other interface (e.g. a website that can take these same parameters and send a request to a backend server for processing) you prefer!

---

##### We recommend using a single SDK to complete this challenge, either the 
* JavaScript https://github.com/tozny/js-sdk, 
* Python https://github.com/tozny/e3db-python
* Java https://github.com/tozny/e3db-java 

as these have the most documentation, examples, and support for all the features you might need to complete this task.

--- 

##### As a prerequisite to using the SDKs you’ll want to sign up for a Tozny account 

* [ ] (https://dashboard.tozny.com/register) in order to create registration tokens
* [ ] (https://www.youtube.com/watch?v=L5ieMF9JZOg) that you will need to create
* [ ] (https://www.youtube.com/watch?v=zb7JS209yHU) and configure
* [ ] (https://www.youtube.com/watch?v=8dMUxKNjNgw) Tozny clients for each of the three parties